# Agenda Churras

## Tecnologias
1. [Express](https://expressjs.com/)
2. [React](https://reactjs.org/)
3. [Redux](https://redux.js.org/)
4. [Styled Components](https://styled-components.com/)
5. [Tachyons](https://tachyons.io/)
6. [Folktale](https://github.com/origamitower/folktale)
6. [MongoDB](https://www.mongodb.com/)
6. [Mongoose](https://mongoosejs.com/)


## Requerimentos
* Node >= v14

## Demo

Demonstração da [aplicação](https://churras-trinca.herokuapp.com).

## Funcionalidades
- [X] CRUD de churras - o backend está pensado para atender um CRUD simples com rotas relacionada aos eventos da agenda
- [ ] Autenticação de usuário - no frontend foi desenvolvido a tela de login na rota /login, porém não é operacional com o backend
- [X] Usuário pode criar eventos
- [X] Usuário pode remover um evento
- [X] Usuário pode adicionar um convidado a um evento
- [ ] Usuário pode remover um convidado de um evento
- [X] Usuário pode mudar confirmação de um usuário 
- [X] Usuário pode ver eventos
- [X] Usuário pode ver convidados, total de arrecadações, descrição e data de um evento

## Build
npm i

npm run build

## Start
O backend usa MongoDB para armazenar os apontamentos. Deve se configurar um mongodb server e setar a string de conexão para a variável de ambiente MONGO_DB_CONNECTION_URL antes de iniciar a aplicação.

Por padrão o servidor irá rodar na porta 80, caso tenha intenção de mudar a porta, sete uma variável de ambiente PORT com o valor desejado.

Comando de inicialização:

npm start

## Desenvolvimento
Sete uma variável de ambiente MONGO_DB_CONNECTION_URL com a string de conexão para o banco MongoDB.

Sete uma variável de ambiente REACT_APP_CHURRAS_API com o host da máquina juntamente com a porta em que está rodando o backend. Por exemplo:

export REACT_APP_CHURRAS_API="http://127.0.0.1:3030"

Comandos de inicialização:
npm i

npm run start_frontend

npm run start_backend


## Scripts
* npm start - Iniciar o servidor da aplicação com os arquivos de build

* npm run start_backend - Iniciar o servidor da aplicação com Watcher e autoreload

* npm run start_frontend - Iniciar um servidor de desenvolvimeto para o frontend com Watcher e autoreload

* npm run build - Iniciar rotina de build dos arquivos estáticos
