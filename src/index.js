import React from "react"
import ReactDOM from "react-dom"
import App from "./frontend/pages/Main/"
import configureStore from "./frontend/configureStore";
import "tachyons/css/tachyons.min.css"

ReactDOM.render(
  <React.StrictMode>
    <App store={configureStore()} />
  </React.StrictMode>,
  document.getElementById("root")
)
