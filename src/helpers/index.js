import Maybe from "folktale/maybe/index.js"

export const K = x => _ => x

export const I = x => x

export const bind = (f, ...args) => x => f(...args.concat(x))

export const exec = f => f()

export const execWith = x => f => f(x)

export const doNothing = K(null)

export const concat = x => y => x.concat(y)

export const fromEmpty = xs => xs.length <= 0 ? Maybe.Nothing() : Maybe.Just(xs)

export const isDevelopment = () => (
  process.env.NODE_ENV === "development"
)

export const sleep = time => (...args) => (
  new Promise(res => (
    setTimeout(res, time, ...args)
  ))
)


export const pluck = (...keys) => obj => (
  keys.reduce((mResult, key) => (
    mResult.chain(result => Maybe.fromNullable(result[key]))
  ), Maybe.fromNullable(obj))
)

export const maybeToPromise = (mx, errorMessage="Maye to Promise rejected") => (
  mx.matchWith({
    Just: ({ value }) => Promise.resolve(value),
    Nothing: () => Promise.reject(errorMessage)
  })
)

export const log = (msg="app message") => x => {
  console.log(`## ${msg} - `, x)
  return x
}

export const logMessage = msg => x => {
  console.log(`## app message - ${msg}`)
  return x
}

export const mapLog = (f, msg="app message") => x => {
  console.log(`## ${msg} - `, f(x))
  return x
}

export const responseOk = res => data => res.json({
  success: true,
  data
})

export const responseFail = res => data => {
  res.status(403)
  res.json({
    success: false,
    error: data.toString()
  })
}

export const withoutProperties = (...keys) => obj => (
  Object.keys(obj).reduce((result, objectKey) => (
    keys.some(key => key === objectKey)
      ? result
      : Object.assign(result, { [objectKey]: obj[objectKey] })
  ), {})
)

export const formatDate = date => (
  new Date(date).toLocaleDateString("pt-BR").replace(`/${new Date().getFullYear()}`, "")
)

export const checkRequiredValues = (required, values) => (
  required.reduce((result, req) => (
    result || pluck(req)(values).map(x => x === "" || typeof x === "number" && isNaN(x)).getOrElse(true)
  ), false)
)
