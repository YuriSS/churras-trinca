import React from "react"
import { Page } from "../../components/Containers"
import { useHistory } from "react-router-dom"
import { getSelectedBarbecue } from "../../reducers/barbecues/selectors"
import { connect } from "react-redux"
import { formatDate, checkRequiredValues, fromEmpty } from "../../../helpers"
import { Money, People } from "../../components/Icons"
import { Date, Value } from "../Home/extra"
import { Just } from "folktale/maybe"
import { Text, Checkbox } from "../../components/Inputs/"
import Button from "../../components/Button/"
import styled from "styled-components"
import { updateGuests, removeEvent, toggleConfirmGuest } from "../../actions/barbecues"
import { useFormValues } from "../../reducers/hooks"
import GoBack from "../../components/GoBack"


const Event = ({ mEvent, updateGuests, removeEvent, toggleConfirmGuest }) => {
  let history = useHistory()
  const [ values, changeValue ] = useFormValues()

  return mEvent.matchWith({
    Nothing: () => (history.push("/home"), null),
    Just: ({ value : event }) => (
      <Page isContent>
        <GoBack />
        <section className="flex justify-between">
          <div>
            <Date>{ formatDate(event.date) }</Date>
            <h2>{ event.description }</h2>
          </div>
          <div>
            <div><People /> <Value>{event.guests.length}</Value></div>
            <div><Money /> <Value>R$ {event.total}</Value></div>
          </div>
        </section>

        <section className="cf">
          <SubTitles>Adicionar convidado</SubTitles>
          <div className="flex justify-between">
            <Text onChange={changeValue("name")} className="w-100" label={Just("Nome")} />
            <Text onChange={changeValue("contribution")} className="ml2 w-100" label={Just("contribution")} type="number" />
          </div>
          <Button
            disabled={checkRequiredValues(["name", "contribution"], values)}
            onClick={() => updateGuests(event.guests.concat(values), event._id)}
            className="mt2 fr"
          >adicionar</Button>
        </section>

        <section className="mt5">
          <SubTitles>Lista de conviadados - <span className="normal i">convidados selecionados foram confirmados</span></SubTitles>
          <ul className="list pa0">
            { fromEmpty(event.guests)
                .matchWith({
                  Nothing: () => <li className="mv4">Nenhum convidado foi incluído para esse evento</li>,
                  Just: ({ value : guests }) => guests.map((guest, i) => (
                    <li key={i} className="flex justify-between items-center bb border-yellow pv3">
                      <Checkbox onClick={() => toggleConfirmGuest(guest._id)} checked={guest.isConfirmed} />
                      <span className="grow-1 mh3">{ guest.name }</span>
                      <span className={`${guest.isConfirmed ? "strike yellow" : ""} b`}>R$ { guest.contribution }</span>
                    </li>
                  ))
                })
            }
          </ul>
        </section>

        <Button danger block onClick={() => removeEvent(event._id).then(() => history.push("/home"))}>Remover evento</Button>
      </Page>
    )
  })
}

export default connect(
  state => ({
    mEvent: getSelectedBarbecue(state)
  }),
  { updateGuests, removeEvent, toggleConfirmGuest }
)(Event)


const SubTitles = styled.h3.attrs({
  className: "bb border-gray "
})``
