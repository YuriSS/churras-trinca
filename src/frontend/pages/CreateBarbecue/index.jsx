import React from "react"
import Button from "../../components/Button/"
import { Just } from "folktale/maybe"
import { Page } from "../../components/Containers"
import { Text, FormControl } from "../../components/Inputs/"
import { K, checkRequiredValues } from "../../../helpers/"
import { createBarbecue } from "../../actions/barbecues"
import { connect } from "react-redux"
import { useHistory } from "react-router-dom"
import { useFormValues } from "../../reducers/hooks"
import GoBack from "../../components/GoBack"


const CreateBarbecue = ({ createBarbecue }) => {
  let history = useHistory()
  const [ values, changeValue ] = useFormValues()

  return (
    <Page isContent>
      <GoBack />
      <h2>Vamos agendar um churras?</h2>
      <FormControl>
        <Text onChange={changeValue("date")} label={Just("Data do evento")} type="date" placeholder="Coloque a data que ocorrerá o evento" />
        <Text onChange={changeValue("description")} label={Just("Descrição")} placeholder="Adicione um título para o evento" />
        <Text onChange={changeValue("notes")} label={Just("Observações adicionais")} placeholder="Preencha com algo que ache relevante para descrever o evento" />
        <Button
          disabled={checkRequiredValues(requiredInputs, values)}
          className="mt4"
          block
          onClick={() => createBarbecue(values).then(() => history.push("/home"))}
        >Criar evento</Button>
      </FormControl>
    </Page>
  )
}

export default connect(
  state => K({}),
  { createBarbecue }
)(CreateBarbecue)

const requiredInputs = [ "date", "description" ]
