import React from "react"
import { Text, FormControl } from "../../components/Inputs"
import Button from "../../components/Button"
import { Just } from "folktale/maybe"
import { Page } from "../../components/Containers"


const Login = () => (
  <Page noBgWhite>
    <FormControl width={280}>
      <Text label={Just("Login")} placeholder="e-mail"/>
      <Text type="password" label={Just("Senha")} placeholder="senha"/>
      <Button block className="mt5">Entrar</Button>
    </FormControl>
  </Page>
)

export default Login
