import React from "react"
import Styles from "../../components/Styles"
import Page from "./Page"
import { Provider } from "react-redux";


const Main = ({ store }) => (
  <Provider store={store}>
    <Styles />
    <Page />
  </Provider>
)

export default Main