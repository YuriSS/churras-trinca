import Background from "../../components/Background"
import Loading from "../../components/Loading"
import Login from "../Login"
import Home from "../Home"
import CreateBarbecue from "../CreateBarbecue/"
import Event from "../Event/"
import React from "react"
import styled from "styled-components"
import { Content } from "../../components/Containers"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import { isLoading } from "../../reducers/barbecues/selectors"
import { connect } from "react-redux"

const Page = ({ isLoading }) => (
  <>
    <Loading show={isLoading} />
    <Background />
    <Content width={636}>
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={Login} />
          <Route path="/home" component={Home} />
          <Route path="/create-barbecue" component={CreateBarbecue} />
          <Route path="/event" component={Event} />
        </Switch>
      </Router>
    </Content>
  </>
)

export default connect(
  state => ({
    isLoading: isLoading(state)
  })
)(Page)


