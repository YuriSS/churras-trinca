import styled from "styled-components"

export const Grid = styled.div.attrs({
  className: "dg gc2 pa1"
})``

export const Block = styled.div.attrs({
  className: "pa3 ma2 bg-white shadow-2 pointer"
})``

export const CreateBlock = styled(Block).attrs({
  className: "bg-gray f3 b tc"
})``

export const Date = styled.div.attrs({
  className: "eb f3 mb2"
})``

export const Description = styled.div.attrs({
  className: "b f4 mb5"
})``

export const Value = styled.div.attrs({
  className: "dib ml1 v-mid f3"
})``
