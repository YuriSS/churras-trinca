import React from "react"
import { Page } from "../../components/Containers"
import { Money, People, Grill } from "../../components/Icons"
import { Grid, Block, CreateBlock, Date, Description, Value } from "./extra"
import { connect } from "react-redux"
import { getBarbecuesData } from "../../reducers/barbecues/selectors";
import { getBarbecues, selectBarbecue } from "../../actions/barbecues";
import { formatDate, doNothing, bind } from "../../../helpers"
import { useHistory } from "react-router-dom"


const Home = ({ getCards, cards, selectCard }) => {
  let history = useHistory()

  React.useEffect(() => {
    cards.matchWith({
      Just: doNothing,
      Nothing: getCards
    })
  }, [ cards, getCards ])

  return (
    <Page>
      <Grid>
        { cards.getOrElse([]).map(card => (
            <Block onClick={() => Promise.resolve(selectCard(card._id)).then(() => history.push("/event"))} key={card._id}>
              <Date>{formatDate(card.date)}</Date>
              <Description>{card.description}</Description>
              <div className="flex justify-between">
                <div><People /> <Value>{card.guests.length}</Value></div>
                <div><Money /> <Value>R$ {card.total}</Value></div>
              </div>
            </Block>
          ))
        }
        <CreateBlock onClick={() => history.push("/create-barbecue")}><Grill /><div className="mt3">Adicionar Churras</div></CreateBlock>
      </Grid>
    </Page>
  )
}


export default connect(
  state => ({
    cards: getBarbecuesData(state),
  }),
  { getCards: getBarbecues, selectCard : selectBarbecue }
)(Home)
