import { isDevelopment, sleep, concat } from "../../helpers"
import Maybe from "folktale/maybe/maybe"
import { getBarbecuesData, getSelectedBarbecue } from "../reducers/barbecues/selectors"

const SCOPE = "barbecues"

export const RECEIVED = `${SCOPE}.RECEIVED`
export const received = data => ({
  type: RECEIVED,
  data
})

export const ERROR_RECEIVED = `${SCOPE}.ERROR_RECEIVED`
export const errorReceived = error => ({
  type: ERROR_RECEIVED,
  error
})

export const REQUESTING = `${SCOPE}.REQUESTING`
export const requesting = () => ({
  type: REQUESTING
})

export const SELECT_BARBECUE = `${SCOPE}.SELECT_BARBECUE`
export const selectBarbecue = id => ({
  type: SELECT_BARBECUE,
  id
})

export const DESELECT_BARBECUE = `${SCOPE}.DESELECT_BARBECUE`
export const deselectBarbecue = () => ({
  type: DESELECT_BARBECUE,
})


export const getBarbecues = () => dispatch => (
  Promise.resolve(dispatch(requesting()))
    .then(() => (
      fetch(`${HOST}/api/barbecue`, {
        headers: {
          "Content-Type": "application/json"
        }
      })
    ))
    .then(res => res.json())
    .then(sleep(1000))
    .then(
      data => dispatch(received(data)),
      error => dispatch(errorReceived(error)),
    )
)

export const createBarbecue = values => dispatch => (
  Promise.resolve(dispatch(requesting()))
    .then(() => (
      fetch(`${HOST}/api/barbecue`, {
        method: "post",
        body: JSON.stringify(values),
        headers: {
          "Content-Type": "application/json",
        }
      })
    ))
    .then(res => res.json())
    .then(() => dispatch(getBarbecues()))
)

export const updateGuests = (guests, id) => (dispatch, getState) => (
  Promise.resolve(dispatch(requesting()))
    .then(() => (
      fetch(`${HOST}/api/barbecue/${id}`, {
        method: "put",
        body: JSON.stringify({ guests }),
        headers: {
          "Content-Type": "application/json",
        }
      })
    ))
    .then(res => res.json())
    .then(() => dispatch(getBarbecues()))
)

export const removeEvent = id => dispatch => (
  Promise.resolve(dispatch(requesting()))
    .then(() => (
      fetch(`${HOST}/api/barbecue/${id}`, {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
        }
      })
    ))
    .then(res => res.json())
    .then(() => dispatch(getBarbecues()))

)

export const toggleConfirmGuest = (id, eventId) => (dispatch, getState) => (
  getSelectedBarbecue(getState())
    .map(barbecue => ({
      id: barbecue._id,
      guests: barbecue.guests
        .map(guest => guest._id !== id ? guest : { ...guest, isConfirmed: !guest.isConfirmed })
    }))
    .matchWith({
      Nothing: () => Promise.reject("Guest not found"),
      Just: ({ value }) => Promise.resolve(value)
    })
    .then(barbecue => dispatch(updateGuests(barbecue.guests, barbecue.id)))
)

const HOST = isDevelopment() ? process.env.REACT_APP_CHURRAS_API : ""
