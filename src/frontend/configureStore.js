import logger from "redux-logger"
import rootReducer from "./reducers/"
import thunk from "redux-thunk"
import { createStore, applyMiddleware } from "redux"
import { isDevelopment } from "../helpers/"

const mids = [ thunk ]

if (isDevelopment()) mids.push(logger)

export const configureStore = initState => createStore(
    rootReducer,
    initState,
    applyMiddleware(...mids)
)

export default configureStore
