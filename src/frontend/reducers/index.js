import { combineReducers } from "redux";
import barbecues from "./barbecues/";


export default combineReducers({
  barbecues
})
