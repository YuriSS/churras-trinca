import React from "react"
import { pluck, exec } from "../../helpers/"

export const useFormValues = (init={}) => {
  const [ values, setValues ] = React.useState(init)

  const changeValue = name => ({ value, type }) => (
    pluck(type)({
      "date": () => ({ name, value: new Date(value).getTime() }),
      "text": () => ({ name, value }),
      "number": () => ({ name, value: Number(value) }),
    })
    .map(exec)
    .map(value => ( setValues({ ...values, [value.name]: value.value }) ))
  )

  return [ values, changeValue, setValues ]
}