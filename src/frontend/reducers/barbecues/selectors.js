import Maybe from "folktale/maybe"

export const getBarbecues = state => (
  state.barbecues
)

export const getBarbecuesData = state => (
  getBarbecues(state).data
)

export const getBarbecuesError = state => (
  getBarbecues(state).error
)

export const isLoading = state => (
  getBarbecues(state).loading
)

export const getSelectedBarbecue = state => (
  Maybe.of(barbecues => selected => (
    barbecues.find(barbecue => barbecue._id === selected)
  ))
  .ap(getBarbecuesData(state))
  .ap(getBarbecues(state).selectedBarbecue)
  .chain(Maybe.fromNullable)
)
