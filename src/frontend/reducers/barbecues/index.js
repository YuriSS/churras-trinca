import { Nothing } from "folktale/maybe"
import { K, pluck, execWith } from "../../../helpers"
import { combineReducers } from "redux"
import {
  RECEIVED as BARBECUES_RECEIVED,
  ERROR_RECEIVED as BARBECUES_ERROR_RECEIVED,
  REQUESTING as BARBECUES_REQUESTING,
  SELECT_BARBECUE,
  DESELECT_BARBECUE
} from "../../actions/barbecues"


const data = (state=Nothing(), action) => (
  pluck(action.type)({
    [BARBECUES_RECEIVED]: pluck("data", "data"),
  })
  .map(execWith(action))
  .getOrElse(state)
)

const error = (state=Nothing(), action) => (
  pluck(action.type)({
    [BARBECUES_RECEIVED]: K(Nothing()),
    [BARBECUES_ERROR_RECEIVED]: pluck("error"),
  })
  .map(execWith(action))
  .getOrElse(state)
)

const loading = (state=false, action) => (
  pluck(action.type)({
    [BARBECUES_REQUESTING]: K(true),
    [BARBECUES_RECEIVED]: K(false),
    [BARBECUES_ERROR_RECEIVED]: K(false),
  })
  .map(execWith(action))
  .getOrElse(state)
)

const selectedBarbecue = (state=Nothing(), action) => (
  pluck(action.type)({
    [SELECT_BARBECUE]: pluck("id"),
    [DESELECT_BARBECUE]: K(Nothing())
  })
  .map(execWith(action))
  .getOrElse(state)
)


export default combineReducers({
  data,
  error,
  loading,
  selectedBarbecue
})
