import React from "react"
import styled from "styled-components"
import { Nothing } from "folktale/maybe"
import { I } from "../../../helpers/"
import { Icon } from "../Icons/"
import { yellow, white } from "../Styles/colors"

export const Text = ({
  label=Nothing(),
  onChange=(() => null),
  type="text",
  name,
  placeholder,
  value,
  className="",
}) => (
  <div className={className}>
    { label.map(l => <Label>{l}</Label>).getOrElse() }
    <InputText value={value} type={type} placeholder={placeholder} onChange={evt => onChange({ value: evt.target.value, name, type })} />
  </div>
)

export const Select = ({
  children=[],
  label=Nothing(),
  onChange=(() => null),
  type="text",
  className=""
}) => (
  <div className={className}>
    { label.map(l => <Label>{l}</Label>).getOrElse() }
    <InputSelect onChange={evt => onChange(evt.target.value)}>
      { children }
    </InputSelect>
  </div>
)


export const FormControl = styled.div`
  margin: auto;
  ${props => props.width ? `width: ${props.width}px;` : ""}
  @media (max-width: 400px) {
    width: 90%
  }
  & div[data-label] {
    margin-top: 2rem;
    &::first-child {
      margin-top: 0;
    }
  }
`

export const Checkbox = styled.div.attrs({
  className: "br-100 ba bw2 border-yellow pointer"
})`
  width: 22px;
  height: 22px;
  background-color: ${props => props.checked ? yellow : white };
`
export default Text


const Label = styled.div.attrs({
  className: "mb1 b",
  "data-label": true
})``

const InputText = styled.input.attrs({
  className: "bg-white br2 ba ph3 pv2 w-100 border-gray"
})`
  min-width: 100px;
`

const InputSelect = styled.select.attrs({
  className: "bg-white br2 bn ph3 pv2 w-100"
})``

