import MoneyImage from "./money.svg"
import PeopleImage from "./people.svg"
import GrillImage from "./grill.svg"
import UtensilsImage from "./utensils.svg"
import ArrowBackImage from "./arrow-left.svg"
import styled from "styled-components"

const SmallIcon = styled.div.attrs({
  className: "dib v-mid contain no-repeat"
})`
  background-image: url(${props => props.icon});
  width: 16px;
  height: 16px;
`

const Icon = styled(SmallIcon).attrs({
  className: "yellow"
})`
  width: 90px;
  height: 90px;
`

const RoundedIcon = styled(Icon).attrs({
  className: "bg-yellow ba bw4 yellow br-100"
})`
`

export const People = styled(SmallIcon).attrs({
  icon: PeopleImage
})``

export const Money = styled(SmallIcon).attrs({
  icon: MoneyImage
})``

export const Grill = styled(RoundedIcon).attrs({
  icon: GrillImage
})``

export const Utensils = styled(Icon).attrs({
  icon: UtensilsImage
})``

export const ArrowBack = styled(Icon).attrs({
  icon: ArrowBackImage
})``
