import React from "react"
import styled from "styled-components"
import { BgWhite } from "./Background/"


export const Fixed = styled.div.attrs({
  className: "fixed top-0 left-0"
})`
  ${props => (
    props.fullscreen
      ? "width: 100%; height: 100%;"
      : ""
  )}
`

export const Absolute = styled.div.attrs({
  className: "absolute top-0 left-0"
})``

export const Content = styled(Absolute).attrs({
})`
  max-width: ${props => props.width}px;
  width: 100%;
  z-index: 1000;
  ${props => (`
    @media (min-width: ${props.width}px) {
      left: 50%;
      margin-left: ${(props.width / 2) * -1}px;
    }
  `)}
  ${props => (
    !props.height
      ? ""
      : `top: 50%; height: ${props.height}px; margin-top: ${(props.height / 2) * -1}px;`
  )}
  ${props => (
    !props.top
      ? ""
      : `top: ${props.top};`
  )}
`
export const StaticContent = styled.div.attrs({
  className: "ph4 pv3"
})``


export const Page = ({ children, flex, noBgWhite=false, isContent=false }) => (
  <>
    <BgWhite className={ noBgWhite ? "dn" : "db" } space="200px" />
    <Content top="-30px">
      <Title>Agenda do Churras</Title>
      <StaticContent className={ `${!isContent ? "" : "bg-white"}` }>
        { children }
      </StaticContent>
    </Content>
  </>
)

const Title = styled.div.attrs({
  className: "tc f2 pa3 eb mv5"
})``
