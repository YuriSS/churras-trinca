import React from "react"
import styled from "styled-components"
import Image from "./image.svg"
import { Fixed } from "../Containers"

export const Background = () => (
  <>
    <BgOpacity />
    <BgFood />
  </>
)
 
export default Background;

export const BgOpacity = styled(Fixed).attrs({
  fullscreen: true
})`
  background: linear-gradient(180deg, rgba(255,216,54,0) 0%, rgba(255,216,54,0.49343487394957986) 20%, rgba(255,216,54,1) 40%, rgba(255,216,54,1) 100%);
  z-index: 10;
`

export const BgFood = styled(Fixed).attrs({
  className: "bg-yellow",
  fullscreen: true
})`
  background-image: url(${Image});
  background-position: center;
  z-index: 0;
`

export const BgWhite = styled(Fixed).attrs({
  className: "bg-lightGray w-100"
})`
  ${props => (
    props.space
      ? `top: ${props.space}; height: calc(100% - ${props.space});`
      : ""
  )}
  z-index: 20;
`
