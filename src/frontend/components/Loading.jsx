import React from "react"
import styled from "styled-components"
import { Fixed, Content } from "./Containers"
import { ReactComponent as UtensilsImage } from "../components/Icons/utensils.svg"
import { white } from "../components/Styles/colors"


const Loading = ({ show }) => (
  <div className={ show ? "db" : "dn" }>
    <Behind className={ show ? "fade-in" : "fade-out" } />
    <Front className={ show ? "fade-in" : "fade-out" }>
      <Utensils />
      <div className="white">Loading...</div>
    </Front>
  </div>
)

export default Loading


const Behind = styled(Fixed).attrs({
  className: "bg-black",
  fullscreen: true
})`
  z-index: 9999;
`

const Front = styled(Content).attrs({
  className: "tc",
  width: 250,
  height: 50
})`
  z-index: 99999;
`

const Utensils = styled(UtensilsImage)`
  width: 30px;
  color: ${white};
`
