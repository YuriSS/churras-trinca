import React from "react"
import { ArrowBack } from "./Icons/"
import { Content } from "./Containers"
import styled from "styled-components"
import { useHistory } from "react-router-dom"

const GoBack = () => {
  const history = useHistory()

  return (
    <Wrapper onClick={() => history.goBack()}>
      <Back />
    </Wrapper>
  )
}

export default GoBack


const Wrapper = styled(Content).attrs({
  className: "pointer"
})`
  top: 75px!important;
  left: 10px!important!;
  z-index: 99999999999;
`

const Back = styled(ArrowBack)`
  width: 20px;
`
