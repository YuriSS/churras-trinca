import styled from "styled-components";
import { gray, red } from "../Styles/colors";

export const Primary = styled.button.attrs({
  className: "bg-black border-black ba bw2 br4 ph5 pv2 white b pointer"
})`
  ${props => (
    !props.block
      ? ""
      : "display: block; width: 100%;"
  )}

  ${props => (
    !props.danger
      ? ""
      : `background-color: ${red}!important; border-color: ${red}!important;`
  )}

  &:disabled {
    background-color: ${gray};
    cursor: not-allowed;
    border-color: ${gray};
  }
`

export default Primary