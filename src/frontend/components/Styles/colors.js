
export const black = "#000"

export const white = "#fff"

export const yellow = "#FFD836"

export const lightGray = "#fafafa"

export const gray = "#f1f1f1"

export const red = "#a71f1f"


export const createColorClasses = () => (
  [["black", black], ["white", white], ["yellow", yellow], ["lightGray", lightGray], ["gray", gray], ["red", red]]
    .reduce((acc, [ clsName, clsValue ]) => acc.concat(`
      .${clsName} {
        color: ${clsValue};
      }
      .bg-${clsName} {
        background-color: ${clsValue};
      }
      .border-${clsName} {
        border-color: ${clsValue};
      }
    `), "")
)
