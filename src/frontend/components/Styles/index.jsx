import { createGlobalStyle } from "styled-components"
import { createColorClasses, black, white } from "./colors"


const GlobalStyles = createGlobalStyle`
  * {
    box-sizing: border-box;
  }
  html {
    font-size: 14px;
  }
  body {
    font-family: 'Raleway', sans-serif;
  }
  .no-outline {
    outline: none;
  }
  .pointer {
    cursor: pointer;
  }
  .f1 {
    font-size: 5rem;
  }
  .eb {
    font-weight: 800;
  }
  .dg {
    display: grid;
  }
  .gc2 {
    grid-template-columns: 50% 50%;
  }
  .shadow-2 {
    box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.06);
  }
  .no-repeat {
    background-repeat: no-repeat;
  }
  .ma {
    margin: auto;
  }
  .grow-1 {
    flex-grow: 1;
  }
  @keyframes fade-in {
    0% { opacity: 0; }
    100% { opacity: .7; }

  }
  @keyframes fade-out {
    0% { opacity: .7; }
    100% { opacity: 0; }
  }
  .fade-in {
    animation: fade-in 0.3s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
  }
  .fade-out {
    animation: fade-out 0.3s cubic-bezier(0.390, 0.575, 0.565, 1.000) both;
  }
  ${createColorClasses()}
`

export default GlobalStyles

export const primary = black

export const secondary = white

export const text = white
