import mongoose from "mongoose"
import { schemaObject as guestSchemaObject } from "../guest/"

const { Schema, model : mongooseModel } = mongoose

const schema = new Schema({
  date: { type: Date, required: true },
  description: { type: String, required: true },
  guests: [ guestSchemaObject ],
  notes: String,
})

const model = mongooseModel("Barbecue", schema)


export default model
