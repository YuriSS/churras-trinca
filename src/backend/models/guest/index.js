export const schemaObject = {
  name: String,
  contribution: Number,
  isConfirmed: Boolean,
  _id: String
}

export default schemaObject
