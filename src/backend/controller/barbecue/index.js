import Barbecue from "../../models/barbecue/index.js"
import { withoutProperties } from "../../../helpers/index.js"
import { v4 as uuidv4 } from "uuid"

export const read = () => (
  Barbecue.find()
    .then(schedule => (
      schedule.map(appointment => ({
        _id: appointment._id,
        guests: appointment.guests,
        description: appointment.description,
        date: appointment.date,
        notes: appointment.notes,
        total: appointment.guests.reduce((res, guest) => res + guest.contribution, 0)
      }))
    ))
)

export const create = data => (
  Promise.resolve(new Barbecue(data))
    .then(model => model.save())
)

export const remove = data => (
  Barbecue.find({ _id: data.id }).deleteOne()
)

export const update = data => (
  Barbecue.updateOne({ _id: data.id }, {
    ...withoutId(data),
    guests: data.guests.map(guest => ({
      ...guest,
      _id: guest._id || uuidv4(),
      isConfirmed: !!guest.isConfirmed
    }))
  })
)


const withoutId = withoutProperties("id")
