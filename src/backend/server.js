import apiRoutes from "./routes/index.js"
import bodyParser from "body-parser"
import cors from "cors"
import express from "express"
import path from "path"
import mongoose from "mongoose"
import { K, bind, pluck, maybeToPromise, log, logMessage } from "../helpers/index.js"


const PORT = pluck("env", "PORT")(process).getOrElse(80)

const STATIC_PATH = path.resolve(__dirname, "../../build/")

const app = express()

const connect = url => (
  mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
)

app.use(cors({ origin: true }))

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({ extended: false }))

app.use(express.static(STATIC_PATH))

app.use("/api", apiRoutes)

app.get("*", function(req, res) {
  res.sendFile("index.html", { root: STATIC_PATH }, res.end.bind(res))
})

maybeToPromise(pluck("env", "MONGO_DB_CONNECTION_URL")(process))
.then(log("mongodb url connection"))
.then(logMessage("Conecting to mongodb server"))
.then(connect)
.then(logMessage("mongodb connected"), log("Mongoose connection error"))


const server = app.listen(PORT, function() {
  console.log(`Server is running on ${server.address().port}`)
})
