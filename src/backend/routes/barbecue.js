import express from "express"
import { responseOk, responseFail } from "../../helpers/index.js"
import {
  create as createBarbecue,
  read   as readBarbecues,
  update as updateBarbecue,
  remove as deleteBarbecue,
} from "../controller/barbecue/index.js"


const router = express.Router()


router.get("/", (req, res) => {
  readBarbecues()
    .then(responseOk(res), responseFail(res))
})

router.post("/", (req, res) => {
  createBarbecue(req.body)
    .then(responseOk(res), responseFail(res))
})

router.delete("/:id", (req, res) => {
  deleteBarbecue({ id: req.params.id })
    .then(responseOk(res), responseFail(res))
})

router.put("/:id", (req, res) => {
  updateBarbecue({ id: req.params.id, ...req.body })
    .then(responseOk(res), responseFail(res))
})


export default router
