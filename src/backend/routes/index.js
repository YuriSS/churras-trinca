import express from "express"
import { pluck } from "../../helpers/index.js"
import barbecueRoutes from "./barbecue.js"


const router = express.Router()

const getName = pluck("name")

const getVersion = pluck("version")


router.get("/", function(req, res) {
  res.json({
    api: getName({}).getOrElse("Churras Trinca API"),
    version: getVersion({}).getOrElse("0.0.0")
  })
})

router.use("/barbecue", barbecueRoutes)


export default router
